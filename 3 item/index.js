'use strict';
function iqTest(num){
    let arr = num.split(' ');
    let result = [];
    arr = arr.map(function (val) {
        if (typeof val == 'string'){
            return +val;
        }else {
            return val;
        }
    });
    arr.forEach(function (val, i) {
        if (val % 2 == 0){
            result.push(i + 1);
        }
    });
    return result;

}

var iqTest1 = iqTest("1 2 1 1 33 40");
console.log(iqTest1);